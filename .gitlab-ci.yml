stages:
  - build
  - test-install
  - test-install-custom
  - test-uninstall
  - release

variables:
  BUILD_IMAGE_NAME: "$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA"

build:
  stage: build
  image: docker:stable
  variables:
    LATEST_REF_IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:latest
  services:
    - docker:19.03.5-dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  script:
    - >-
       docker build
       --tag "$LATEST_REF_IMAGE"
       --tag "$BUILD_IMAGE_NAME" .
    - docker push "$BUILD_IMAGE_NAME"
    - docker push "$LATEST_REF_IMAGE"

test-shellcheck:
  # Put into first stage as does not depend on anything
  stage: build
  image: koalaman/shellcheck-alpine:stable
  script:
    - |
      shellcheck src/bin/gitlab-managed-apps     \
                 test/bin/restart-system-pods    \
                 test/managed-apps/cilium/verify

# Shared job to test installing applications w/o customization
# Define GITLAB_MANAGED_APPS_FILE in variables:
.install-applications-all:
  stage: test-install
  script:
    - export INGRESS_VALUES_FILE=MISSING.yaml
    - export SENTRY_VALUES_FILE=MISSING.yaml
    - export CERT_MANAGER_VALUES_FILE=MISSING.yaml
    - export GITLAB_RUNNER_VALUES_FILE=MISSING.yaml
    - export CILIUM_VALUES_FILE=MISSING.yaml
    - export CILIUM_HUBBLE_VALUES_FILE=MISSING.yaml
    - export ELASTIC_STACK_VALUES_FILE=MISSING.yaml
    - export CROSSPLANE_VALUES_FILE=MISSING.yaml
    - export POSTHOG_VALUES_FILE=MISSING.yaml

    - export JUPYTERHUB_VALUES_FILE=MISSING.yaml
    - export JUPYTERHUB_PROXY_SECRET_TOKEN=e3c8cc5f124ad37cb51efea17f6e7b5ca8ceacf9725901f61d28de24bf16076a
    - export JUPYTERHUB_COOKIE_SECRET=059cad46aa0bec9dcbe6e108d8d132229a46ce8762c0764bfc6cba9b5c870498
    - export JUPYTERHUB_HOST=jupyter.example.com
    - export JUPYTERHUB_GITLAB_HOST=gitlab.example.com
    - export JUPYTERHUB_AUTH_CRYPTO_KEY=812a61aed842daea118728d8d8fde5c44c0fcd4496873aa0393664bb13097d6b
    - export JUPYTERHUB_AUTH_GITLAB_CLIENT_ID=client_id
    - export JUPYTERHUB_AUTH_GITLAB_CLIENT_SECRET=client_secret

    - export VAULT_VALUES_FILE=MISSING.yaml
    - export TILLER_NAMESPACE=gitlab-managed-apps
    - export PROMETHEUS_VALUES_FILE=MISSING.yaml
    - gitlab-managed-apps /usr/local/share/gitlab-managed-apps/helmfile.yaml
    - ./test/managed-apps/gitlab-runner/verify
    - ./test/managed-apps/ingress/verify
    - ./test/managed-apps/jupyterhub/verify
    - ./test/managed-apps/prometheus/verify
    - ./test/managed-apps/cilium/verify
    - ./test/managed-apps/elastic-stack/verify
    - ./test/managed-apps/vault/verify
    - ./test/managed-apps/crossplane/verify
    - ./test/managed-apps/posthog/verify
  artifacts:
    when: on_failure
    paths:
      - tiller.log

# Shared job to test installing default applications w/o customization
# Ensures that applications not marked for installation do not leak requirements
.install-applications-default:
  stage: test-install
  variables:
    GITLAB_MANAGED_APPS_FILE: MISSING.yaml
  script:
    - export INGRESS_VALUES_FILE=MISSING.yaml
    - export SENTRY_VALUES_FILE=MISSING.yaml
    - export CERT_MANAGER_VALUES_FILE=MISSING.yaml
    - export GITLAB_RUNNER_VALUES_FILE=MISSING.yaml
    - export CILIUM_VALUES_FILE=MISSING.yaml
    - export CILIUM_HUBBLE_VALUES_FILE=MISSING.yaml
    - export JUPYTERHUB_VALUES_FILE=MISSING.yaml
    - export ELASTIC_STACK_VALUES_FILE=MISSING.yaml
    - export VAULT_VALUES_FILE=MISSING.yaml
    - export CROSSPLANE_VALUES_FILE=MISSING.yaml
    - export POSTHOG_VALUES_FILE=MISSING.yaml
    - export TILLER_NAMESPACE=gitlab-managed-apps

    - gitlab-managed-apps /usr/local/share/gitlab-managed-apps/helmfile.yaml
  artifacts:
    when: on_failure
    paths:
      - tiller.log

# Shared job to test installing applications with customization
# Define GITLAB_MANAGED_APPS_FILE in variables:
.install-applications-with-custom:
  stage: test-install-custom
  script:
    - export INGRESS_VALUES_FILE="$CI_PROJECT_DIR/test/managed-apps/ingress/values.yaml"
    - export SENTRY_VALUES_FILE="$CI_PROJECT_DIR/test/managed-apps/sentry/values.yaml"
    - export CERT_MANAGER_VALUES_FILE="$CI_PROJECT_DIR/test/managed-apps/cert-manager/values.yaml"
    - export GITLAB_RUNNER_VALUES_FILE="$CI_PROJECT_DIR/test/managed-apps/gitlab-runner/values.yaml"
    - export CILIUM_VALUES_FILE="$CI_PROJECT_DIR/test/managed-apps/cilium/values.yaml"
    - export CILIUM_HUBBLE_VALUES_FILE="$CI_PROJECT_DIR/test/managed-apps/cilium/hubble-values.yaml"
    - export ELASTIC_STACK_VALUES_FILE="$CI_PROJECT_DIR/test/managed-apps/elastic-stack/values.yaml"
    - export CROSSPLANE_VALUES_FILE="$CI_PROJECT_DIR/test/managed-apps/crossplane/values.yaml"
    - export POSTHOG_VALUES_FILE="$CI_PROJECT_DIR/test/managed-apps/posthog/values.yaml"

    - export JUPYTERHUB_VALUES_FILE="$CI_PROJECT_DIR/test/managed-apps/jupyterhub/values.yaml"
    - export JUPYTERHUB_PROXY_SECRET_TOKEN=e3c8cc5f124ad37cb51efea17f6e7b5ca8ceacf9725901f61d28de24bf16076a
    - export JUPYTERHUB_COOKIE_SECRET=059cad46aa0bec9dcbe6e108d8d132229a46ce8762c0764bfc6cba9b5c870498
    - export JUPYTERHUB_HOST=jupyter.example.com
    - export JUPYTERHUB_GITLAB_HOST=gitlab.example.com
    - export JUPYTERHUB_AUTH_CRYPTO_KEY=812a61aed842daea118728d8d8fde5c44c0fcd4496873aa0393664bb13097d6b
    - export JUPYTERHUB_AUTH_GITLAB_CLIENT_ID=client_id
    - export JUPYTERHUB_AUTH_GITLAB_CLIENT_SECRET=client_secret

    - export VAULT_VALUES_FILE="$CI_PROJECT_DIR/test/managed-apps/vault/values.yaml"
    - export TILLER_NAMESPACE=gitlab-managed-apps
    - export PROMETHEUS_VALUES_FILE="$CI_PROJECT_DIR/test/managed-apps/prometheus/values.yaml"
    - gitlab-managed-apps /usr/local/share/gitlab-managed-apps/helmfile.yaml
    # - ./test/managed-apps/gitlab-runner/verify-custom
    - ./test/managed-apps/jupyterhub/verify
    - ./test/managed-apps/prometheus/verify-custom
    - ./test/managed-apps/cilium/verify audit
    - ./test/managed-apps/elastic-stack/verify-custom
    - ./test/managed-apps/crossplane/verify-custom
    - ./test/managed-apps/posthog/verify-custom
  artifacts:
    when: on_failure
    paths:
      - tiller.log

# Shared job to test uninstalling applications
# Define GITLAB_MANAGED_APPS_FILE in variables:
.uninstall:
  stage: test-uninstall
  script:
    - export TILLER_NAMESPACE=gitlab-managed-apps
    - gitlab-managed-apps /usr/local/share/gitlab-managed-apps/helmfile.yaml
    - kubectl delete pvc -n gitlab-managed-apps --force --grace-period=0 --selector 'release=sentry'
  artifacts:
    when: on_failure
    paths:
      - tiller.log

.gke-integration:
  image: "$BUILD_IMAGE_NAME"
  environment:
    name: gke

gke:install-applications:
  extends: [".gke-integration", ".install-applications-all"]
  variables:
    GITLAB_MANAGED_APPS_FILE: $CI_PROJECT_DIR/test/managed-apps/gke-config.yaml
  rules:
    - if: '$CI_PROJECT_PATH == "gitlab-org/cluster-integration/cluster-applications" && $CI_COMMIT_REF_NAME == "master"'
      when: on_success
    - if: '$TEST_GKE_CLUSTER'
      when: on_success

gke:install-applications-with-custom:
  extends: [".gke-integration", ".install-applications-with-custom"]
  variables:
    GITLAB_MANAGED_APPS_FILE: $CI_PROJECT_DIR/test/managed-apps/gke-config.yaml
  rules:
    - if: '$CI_PROJECT_PATH == "gitlab-org/cluster-integration/cluster-applications" && $CI_COMMIT_REF_NAME == "master"'
      when: on_success
    - if: '$TEST_GKE_CLUSTER'
      when: on_success

gke:uninstall:
  extends: [".gke-integration", ".uninstall"]
  rules:
    - if: '$CI_PROJECT_PATH == "gitlab-org/cluster-integration/cluster-applications" && $CI_COMMIT_REF_NAME == "master"'
      when: always
    - if: '$TEST_GKE_CLUSTER'
      when: always
  variables:
    GITLAB_MANAGED_APPS_FILE: MISSING.yaml
  after_script:
    - export TILLER_NAMESPACE=gitlab-managed-apps
    - while kubectl -n "$TILLER_NAMESPACE" get po | grep -q "cilium-node-init"; do sleep 1; done
    - test/bin/restart-system-pods

.k3s:
  image: "$BUILD_IMAGE_NAME"
  services:
    - name: registry.gitlab.com/gitlab-org/cluster-integration/test-utils/k3s-gitlab-ci/releases/v0.6.1
      alias: k3s
      command: ["server", "--cluster-secret", "some-secret"]
  before_script:
    - kubectl config set-cluster k3s --server https://k3s:6443 --insecure-skip-tls-verify
    - kubectl config set-credentials default --username=node --password=some-secret
    - kubectl config set-context k3s --cluster=k3s --user=default
    - kubectl config use-context k3s
    - kubectl version

k3s:install-applications-default:
  extends: [".k3s", ".install-applications-default"]

k3s:install-applications:
  extends: [".k3s", ".install-applications-all"]
  variables:
    GITLAB_MANAGED_APPS_FILE: $CI_PROJECT_DIR/test/managed-apps/k3s-config.yaml

k3s:install-application-with-custom:
  extends: [".k3s", ".install-applications-with-custom"]
  variables:
    GITLAB_MANAGED_APPS_FILE: $CI_PROJECT_DIR/test/managed-apps/k3s-config.yaml

k3s:install-applications-no-cluster-issuer:
  extends: [".k3s", ".install-applications-all"]
  variables:
    GITLAB_MANAGED_APPS_FILE: $CI_PROJECT_DIR/test/managed-apps/k3s-config-no-cluster-issuer.yaml

k3s:uninstall:
  extends: [".k3s", ".uninstall"]
  variables:
    GITLAB_MANAGED_APPS_FILE: MISSING.yaml

# This template is used for the publish jobs, which do the following:
#   * Check to see if there is a version bump based on
#     [Conventional Commits (v1.0.0-beta.2)](https://www.conventionalcommits.org/en/v1.0.0-beta.2/)
#     See README.md for more information
#   * If there is a new release it will tag the repository with the new release as the `ops-gitlab-net`
#     user

.semantic-release:
  image: node:12
  stage: release
  before_script:
    - npm install -g semantic-release @semantic-release/gitlab
  script:
    - semantic-release $DRY_RUN_OPT -b $CI_COMMIT_REF_NAME
  only:
    variables:
      - $CI_API_V4_URL == "https://gitlab.com/api/v4"

release-tag:
  stage: release
  image: docker:19.03.5
  services:
    - docker:19.03.5-dind
  script:
    - 'echo ${CI_JOB_TOKEN} | docker login --password-stdin -u $CI_REGISTRY_USER $CI_REGISTRY'
    - export ci_image="${CI_REGISTRY_IMAGE}"
    - export ci_image_tag=${CI_COMMIT_TAG:-$CI_COMMIT_SHORT_SHA}
    - echo "Using tag $ci_image_tag for image"
    - docker pull "$BUILD_IMAGE_NAME"
    - docker tag "$BUILD_IMAGE_NAME" $ci_image:latest
    - docker tag "$BUILD_IMAGE_NAME" $ci_image:$ci_image_tag
    - docker push $ci_image:latest
    - docker push $ci_image:$ci_image_tag
  only:
    - tags

publish:
  extends: .semantic-release
  only:
    refs:
      - master@gitlab-org/cluster-integration/cluster-applications

publish-dryrun:
  extends: .semantic-release
  variables:
    DRY_RUN_OPT: '-d'
  only:
    refs:
      - branches@gitlab-org/cluster-integration/cluster-applications
  except:
    refs:
      - master
